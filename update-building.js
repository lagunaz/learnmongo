const mongoose = require('mongoose')
const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
// const Building = require('./models/Building')

async function main () {
  const newInformaticBuilding = await Building.findById('621b5e7b7c65710853beba38')
  const room = await Room.findById('621b5e7b7c65710853beba3d')
  const informaticBuilding = await Building.findById(room.building)
  console.log(newInformaticBuilding)
  console.log(room)
  console.log(informaticBuilding)

  room.building = newInformaticBuilding
  newInformaticBuilding.rooms.push(room)

  informaticBuilding.rooms.pull(room)

  room.save()
  newInformaticBuilding.save()
  informaticBuilding.save()
}

main().then(function () {
  console.log('Finish')
})
