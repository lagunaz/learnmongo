const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function clearDb () {
  await Room.deleteMany({})
  await Building.deleteMany({})
}

async function main () {
  await clearDb()
  const informaticBuilding = new Building({ name: 'Informatics', floor: 11 })
  const newInformaticBuilding = new Building({ name: 'New Informatics', floor: 20 })

  const room3c01 = new Room({ name: '3c01', capacity: 200, floor: 3, building: informaticBuilding })
  informaticBuilding.rooms.push(room3c01)
  const room4c01 = new Room({ name: '4c01', capacity: 150, floor: 4, building: informaticBuilding })
  informaticBuilding.rooms.push(room4c01)
  const room5c01 = new Room({ name: '5c01', capacity: 100, floor: 5, building: informaticBuilding })
  informaticBuilding.rooms.push(room5c01)

  await informaticBuilding.save()
  await newInformaticBuilding.save()
  await room3c01.save()
  await room4c01.save()
  await room5c01.save()
}

main().then(function () {
  console.log('Finish')
})
